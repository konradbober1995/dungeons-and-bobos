#include "enemy.h"

short Enemy::id = 0;

Enemy::Enemy()
{

}

void Enemy::enemy_move(Interface *game)
{
    short n;
    bool mv=false;
    unsigned short mask_b=0;

    do
    {
        n= (int)(rand() / (RAND_MAX + 1.0) * 4 );

        switch (n)
        {
        case 0:
            mask_b=mask_b | 1;
            if(game->colide[pos_y+1][pos_x+2]==' ')
            {
                game->tabl[pos_y][pos_x]=' ';
                game->tab_k[pos_y][pos_x]=1;
                game->colide[pos_y+1][pos_x+1]=' ';
                game->tab_id[pos_y][pos_x]=0;

                game->tabl[pos_y][pos_x+1]=avatar;
                game->tab_k[pos_y][pos_x+1]=enemy_colour;
                game->colide[pos_y+1][pos_x+2]='T';
                game->tab_id[pos_y][pos_x+1]=it;
                pos_x=pos_x+1;
                mv=true;
            }
            break;

        case 1:
            mask_b=mask_b | 2;
            if(game->colide[pos_y+1][pos_x]==' ')
            {
                game->tabl[pos_y][pos_x]=' ';
                game->tab_k[pos_y][pos_x]=1;
                game->colide[pos_y+1][pos_x+1]=' ';
                game->tab_id[pos_y][pos_x]=0;

                game->tabl[pos_y][pos_x-1]=avatar;
                game->tab_k[pos_y][pos_x-1]=enemy_colour;
                game->colide[pos_y+1][pos_x]='T';
                game->tab_id[pos_y][pos_x-1]=it;
                pos_x=pos_x-1;
                mv=true;
            }
            break;

        case 2:
            mask_b=mask_b | 4;
            if(game->colide[pos_y+2][pos_x+1]==' ')
            {
                game->tabl[pos_y][pos_x]=' ';
                game->tab_k[pos_y][pos_x]=1;
                game->colide[pos_y+1][pos_x+1]=' ';
                game->tab_id[pos_y][pos_x]=0;

                game->tabl[pos_y+1][pos_x]=avatar;
                game->tab_k[pos_y+1][pos_x]=enemy_colour;
                game->colide[pos_y+2][pos_x+1]='T';
                game->tab_id[pos_y+1][pos_x]=it;
                pos_y=pos_y+1;
                mv=true;
            }
            break;

        case 3:
            mask_b=mask_b | 8;
            if(game->colide[pos_y][pos_x+1]==' ')
            {
                game->tabl[pos_y][pos_x]=' ';
                game->tab_k[pos_y][pos_x]=1;
                game->colide[pos_y+1][pos_x+1]=' ';
                game->tab_id[pos_y][pos_x]=0;

                game->tabl[pos_y-1][pos_x]=avatar;
                game->tab_k[pos_y-1][pos_x]=enemy_colour;
                game->colide[pos_y][pos_x+1]='T';
                game->tab_id[pos_y-1][pos_x]=it;
                pos_y=pos_y-1;
                mv=true;
            }
            break;
        }

        if(mask_b==15)
        {
            string err="Przeciwnik nie moze wykonac zadnego ruchu";
            throw err;
        }

    }
    while(mv!=true);
    mv=false;

    mask_b=0;
}

Enemy::~Enemy()
{

}

void Enemy::draw_eninfo_win(WINDOW *enemy_info)
{
    wclear(enemy_info);

    mvwprintw(enemy_info,1,1,"%s",nazwa.c_str());
    mvwprintw(enemy_info,3,1,"HP: %d",hp);
    //mvwprintw(enemy_info,5,1,"ID: %d",obj_id);

    wrefresh(enemy_info);
}

void Enemy::enemy_hit(short hit)
{

    if(hp>0)
        hp-=hit;

}

short Enemy::life_cycle(Interface* game,short player_y,short player_x)
{
    if(sqrt((player_x - pos_x)*(player_x - pos_x)+(player_y - pos_y)*(player_y - pos_y))<2)
    {
        return 1;
    }
    else
    {
        enemy_move(game);
        return 0;
    }
}

int Bandit::enemy_atk()
{
    return (int)(rand() / (RAND_MAX + 1.0) * 10 );
}

int Goblin::enemy_atk()
{
    return (int)(rand() / (RAND_MAX + 1.0) * 15 );
}

int Guard::enemy_atk()
{
    return (int)(rand() / (RAND_MAX + 1.0) * 15 ) + 5;
}

Bandit::Bandit(Interface& game)
{
    obj_id=0;
    hp=100;
    pd=20;
    pos_x=20;
    pos_y=15;
    avatar='B';
    enemy_colour=2;
    nazwa="Bandyta";

    pos_y= (int)(rand() / (RAND_MAX + 1.0) *  31);
    pos_x= (int)(rand() / (RAND_MAX + 1.0) *  78);

    game.tabl[pos_y][pos_x]=avatar;
    game.tab_k[pos_y][pos_x]=enemy_colour;
    game.colide[pos_y+1][pos_x+1]='T';
}

Bandit::Bandit(Interface& game,Enemy & ban)
{
    obj_id=0;
    hp=ban.hp;
    pos_x=20;
    pos_y=15;
    avatar='B';
    enemy_colour=2;
    nazwa="Bandyta";

    pos_y= (int)(rand() / (RAND_MAX + 1.0) *  31);
    pos_x= (int)(rand() / (RAND_MAX + 1.0) *  78);

    game.tabl[pos_y][pos_x]=avatar;
    game.tab_k[pos_y][pos_x]=enemy_colour;
    game.colide[pos_y+1][pos_x+1]='T';
}

Goblin::Goblin(Interface* game)
{
    obj_id=0;
    hp=120;
    pd=40;
    pos_x=21;
    pos_y=15;
    avatar='G';
    enemy_colour=7;
    nazwa="Goblin";

    pos_y= (int)(rand() / (RAND_MAX + 1.0) *  31);
    pos_x= (int)(rand() / (RAND_MAX + 1.0) *  78);

    game->tabl[pos_y][pos_x]=avatar;
    game->tab_k[pos_y][pos_x]=enemy_colour;
    game->colide[pos_y+1][pos_x+1]='T';
}

Goblin::Goblin(Interface& game,Enemy & ban)
{
    obj_id=0;
    hp=ban.hp;
    pos_x=20;
    pos_y=15;
    avatar='G';
    enemy_colour=7;
    nazwa="Goblin";

    pos_y= (int)(rand() / (RAND_MAX + 1.0) *  31);
    pos_x= (int)(rand() / (RAND_MAX + 1.0) *  78);

    game.tabl[pos_y][pos_x]=avatar;
    game.tab_k[pos_y][pos_x]=enemy_colour;
    game.colide[pos_y+1][pos_x+1]='T';
}

Guard::Guard(Interface& game)
{
    obj_id=1;
    hp=200;
    pd=100;
    pos_x=21;
    pos_y=15;
    avatar='D';
    enemy_colour=3;
    nazwa="BOSS";

    pos_y= (int)(rand() / (RAND_MAX + 1.0) *  31);
    pos_x= (int)(rand() / (RAND_MAX + 1.0) *  78);

    game.tabl[pos_y][pos_x]=avatar;
    game.tab_k[pos_y][pos_x]=enemy_colour;
    game.colide[pos_y+1][pos_x+1]='T';
}
