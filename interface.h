#ifndef interface_h
#define interface_h

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <time.h>
#include "pdcurses/curses.h"
#include <fstream>
#include <cstring>
#include <bass.h>
#include <string>
#include <math.h>
#include <vector>
#include "enemy.h"
#include "player.h"


using namespace std;

class Enemy;
class Bandit;
class Goblin;
class Player;

class Interface
{
protected:

    short **tab_k;
    short **tab_id;
    char **tabl;
    char **colide;

    class dialogue
    {
    public:
        string txt;
        short col;
        dialogue *next;
    };

    dialogue *lista,*start_pos;

    time_t diff,save_beg;
    time_t czas,cykl,s,beg,zegar;

    float sec;
    float volume;

    bool musiconoff;
    bool fpsonoff;
    bool is_game_loaded;
    bool loopend;

    WINDOW *game_win;
    WINDOW *score;
    WINDOW *dialog;
    WINDOW *enemy_info;

    short GAME_HEIGHT;
    short GAME_WIDTH;
    short WIN_HEIGHT;
    short WIN_WIDTH;
    short fps,f;
    short x,y;

    short minutes,hour; //minuty i godziny gry
    short tmp_sec;
    short atk_sec;

    float seconds; //sekundy gry

    Enemy *bandyta;

    char slot1[20];
    char slot2[20];
    char slot3[20];

    string tekst;

public:
    Interface();
    ~Interface();
    void draw_borders();
    void los(short kolor);
    void draw_game();
    void los_enemy();
    void los_tree();
    void los_grass();
    void generate_tree2();
    void inwentory();
    void battle();
    void ingame_menu(HSTREAM hmus);
    void main_menu();
    void interfac();
    void draw_score_win(Player *bobo);
    void save(char save_name[25]);
    void load(char save_name[25]);
    void menu_save();
    void dial_push(string new_dialogue,short dia_colour);
    void dial_draw();
    void clean();

    void submenu_save_ENTER(char slot[25],short *updown);
    void submenu_save_rename(char slot[25],short *updown);
    void submenu_save_delete(char slot[25],short *updown);
    void call_enemy(Enemy *bandyta);

    friend class Bandit;
    friend class Goblin;
    friend class Guard;
    friend class Enemy;
    friend void show_arrays(Interface &); //funkcja pokazujaca tablice
};
#endif
