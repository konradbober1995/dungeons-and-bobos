#ifndef enemy_h
#define enemy_h
#include "interface.h"

class Interface;

class Enemy
{
protected:
    short enemy_colour;

public:
    static short id;
    short obj_id;
    short it;
    short pd;
    short pos_x;
    short pos_y;
    short hp;

    std::string nazwa;

    char avatar;

    Enemy();
    ~Enemy();
    void enemy_move(Interface *game);
    virtual int enemy_atk() = 0;
    void enemy_hit(short hit);
    void draw_eninfo_win(WINDOW *enemy_info);
    short life_cycle(Interface *game,short player_y,short player_x);
};

class Bandit : public Enemy
{
public:
    Bandit(Interface& );
    Bandit(Interface& ,Enemy &);
    ~Bandit();
    int enemy_atk();
};

class Goblin : public Enemy
{
public:
    Goblin(Interface* game);
    Goblin(Interface& ,Enemy &);
    ~Goblin();
    int enemy_atk();
};

class Guard : public Enemy
{
public:
    Guard(Interface& );
    ~Guard();
    int enemy_atk();
};

#endif

