#include "interface.h"
#include <cstring>

Interface::Interface()
{
    initscr();
    short i,j;
    resize_term(40,100);
    getmaxyx( stdscr, WIN_HEIGHT, WIN_WIDTH );
    GAME_HEIGHT=WIN_HEIGHT-7;
    GAME_WIDTH=WIN_WIDTH-20;

    curs_set(0);
    diff=0;
    volume=0.5;
    musiconoff=false;
    fpsonoff=false;
    is_game_loaded=false;
    loopend=true;
    sec=0;
    minutes=0;
    czas=0;
    cykl=0;
    s=0;
    x=0,y=0;
    save_beg=0;
    lista=NULL;
    start_pos=NULL;

    keypad( stdscr, true );
    srand(time(NULL));

    tabl = new char *[GAME_HEIGHT-2]; //dynamiczne tworzenie tablicy znaków
    for(i=0; i<GAME_HEIGHT-2; i++)
    {
        tabl[i]=new char[GAME_WIDTH-2];
    }

    colide = new char *[GAME_HEIGHT]; //dynamiczne tworzenie tablicy kolizji
    for(i=0; i<GAME_HEIGHT; i++)
    {
        colide[i]=new char[GAME_WIDTH];
    }

    tab_k = new short *[GAME_HEIGHT-2]; //dynamiczne tworzenie tablicy kolorów
    for(i=0; i<GAME_HEIGHT-2; i++)
    {
        tab_k[i]=new short[GAME_WIDTH-2];
    }

    tab_id = new short *[GAME_HEIGHT-2]; //dynamiczne tworzenie tablicy id
    for(i=0; i<GAME_HEIGHT-2; i++)
    {
        tab_id[i]=new short[GAME_WIDTH-2];
    }
    //*********************************************

    for(i=0; i<GAME_HEIGHT-2; i++) //czyszczenie tablicy znaków
        for(j=0; j<GAME_WIDTH-2; j++)
            tabl[i][j]=' ';

    for(i=0; i<GAME_HEIGHT-2; i++) //czyszczenie tablicy kolorów
        for(j=0; j<GAME_WIDTH-2; j++)
            tab_k[i][j]=1;

    for(i=0; i<GAME_HEIGHT; i++) //czyszczenie tablicy kolizji
        for(j=0; j<GAME_WIDTH; j++)
            colide[i][j]=' ';

    for(i=0; i<GAME_HEIGHT-2; i++) //czyszczenie tablicy id
        for(j=0; j<GAME_WIDTH-2; j++)
            tab_id[i][j]=0;

    //*****************KOLIZJA OBRAMOWANIA****************************

    colide[0][0]='T';
    colide[GAME_HEIGHT-1][0]='T';
    colide[0][GAME_WIDTH-1]='T';
    colide[GAME_HEIGHT-1][GAME_WIDTH-1]='T';

    for(i=1; i<GAME_HEIGHT-1; i++)
    {
        colide[i][0]='T';
        colide[i][GAME_WIDTH-1]='T';
    }

    for(i=1; i<GAME_WIDTH-1; i++)
    {
        colide[0][i]='T';
        colide[GAME_HEIGHT-1][i]='T';
    }

}

//**********************************************************************

Interface::~Interface()
{
    int i;

    for(i=0; i<GAME_HEIGHT-2; i++)
    {
        delete []tabl[i];
    }
    for(i=0; i<GAME_HEIGHT; i++)
    {
        delete []colide[i];
    }
    for(i=0; i<GAME_HEIGHT-2; i++)
    {
        delete []tab_k[i];
    }

    delete []tabl;
    delete []colide;
    delete []tab_k;
}

//**********************************************************************

void Interface::main_menu()
{
    bool stop=false;
    short updown=0;
    char key;
    //resize_term(30,80);
    std::fstream load_name;

    noecho();

    do
    {

        if((key=='s'||key==2)&&updown<3)
            updown+=1;
        else if ((key=='w'||key==3)&&updown>0)
            updown-=1;
        clear();
        attron( A_BOLD );
        mvprintw(10+(updown*2),WIN_WIDTH/2-8,"*");
        mvprintw(10,WIN_WIDTH/2-3,"Start");
        mvprintw(12,WIN_WIDTH/2-4,"Wczytaj");
        mvprintw(14,WIN_WIDTH/2-3,"Opcje");
        mvprintw(16,WIN_WIDTH/2-4,"Zako\344cz");
        attroff( A_BOLD );

        key=getch();


        if(updown==0&&key==10)
        {
            interfac();
        }

        if(updown==1&&key==10)
        {
            updown=0;
            key=0;
            load_name.open("save/savelist.dnb",std::ios::in);
            load_name.getline(slot1,20);
            load_name.getline(slot2,20);
            load_name.getline(slot3,20);
            load_name.close();
            do
            {
                clear();
                attron( A_BOLD );

                if((key=='s'||key==2)&&updown<2)
                    updown+=1;
                if ((key=='w'||key==3)&&updown>0)
                    updown-=1;

                mvprintw(10+(updown*2),WIN_WIDTH/2-11,"*");
                if(musiconoff==true)
                    mvprintw(10,WIN_WIDTH/2-8,"->");
                mvprintw(12,WIN_WIDTH/2-8,"->");
                mvprintw(14,WIN_WIDTH/2-8,"->");

                if(strlen(slot1)==0)
                    mvprintw(10,WIN_WIDTH/2-6,"PUSTO");
                else
                    mvprintw(10,WIN_WIDTH/2-6,"%s",slot1);

                if(strlen(slot2)==0)
                    mvprintw(12,WIN_WIDTH/2-6,"PUSTO");
                else
                    mvprintw(12,WIN_WIDTH/2-6,"%s",slot2);

                if(strlen(slot3)==0)
                    mvprintw(14,WIN_WIDTH/2-6,"PUSTO");
                else
                    mvprintw(14,WIN_WIDTH/2-6,"%s",slot3);

                if(updown==0&&key==10&&strlen(slot1)!=0)
                {
                    load(slot1);
                    interfac();
                    key=27;
                }

                if(updown==1&&key==10&&strlen(slot2)!=0)
                {
                    load(slot2);
                    interfac();
                    key=27;
                }

                if(updown==2&&key==10&&strlen(slot3)!=0)
                {
                    load(slot3);
                    interfac();
                    key=27;
                }

                key=getch();

                attroff( A_BOLD );
            }
            while(key!=27);
            key=0;
        }

        if(updown==2&&key==10)
        {
            updown=0;
            do
            {
                clear();
                attron( A_BOLD );

                if((key=='s'||key==2)&&updown<1&&musiconoff==true)
                    updown+=1;
                else if ((key=='w'||key==3)&&updown>0)
                    updown-=1;

                mvprintw(11+(updown*2),WIN_WIDTH/2-12,"*");
                if(musiconoff==true)
                    mvprintw(13,WIN_WIDTH/2-8,"G\210o\230no\230\206:(%2.0f)",volume*100);
                mvprintw(11,WIN_WIDTH/2-8,"Muzyka(0/1):%d",musiconoff);

                key=getch();

                if(musiconoff==true)
                {
                    if(updown==1&&(key=='a'||key==4)&&volume>0.1)
                        volume-=0.10;
                    if(updown==1&&(key=='d'||key==5)&&volume<1)
                        volume+=0.10;
                }
                if(updown==0&&(key=='d'||key==5))
                    musiconoff=true;
                if(updown==0&&(key=='a'||key==4))
                    musiconoff=false;

                attroff( A_BOLD );
            }
            while(key!=27&&key!=10);
        }

        if(updown==3&&key==10)
            stop=true;

    }
    while(!stop);

    endwin();
}

//**********************************************************************

void Interface::inwentory()
{


    char key;
    short updown=0;
    time_t start;

    start=clock();

    do
    {

        if((key=='s'||key==2)&&updown<3)
            updown+=1;
        else if ((key=='w'||key==3)&&updown>0)
            updown-=1;
        clear();
        attron( A_BOLD );
        mvprintw(10+(updown*2),WIN_WIDTH/2-5,"*");
        mvprintw(10,WIN_WIDTH/2-3,"G\210owa");
        mvprintw(12,WIN_WIDTH/2-3,"Tors");
        mvprintw(14,WIN_WIDTH/2-3,"R\251ce");
        mvprintw(16,WIN_WIDTH/2-3,"Nogi");
        attroff( A_BOLD );

        key=getch();


    }
    while(key!=27&&key!='e');

    (diff)=(diff)+difftime(clock(),start);

}

//************************************************************************

void show_arrays(Interface & I) //wyswietlanie tablic używanych w grze
{
    nodelay(stdscr,0);
    clear();
    short i,j;
    for(i=0; i<I.GAME_HEIGHT-2; i++)
    {
        for(j=0; j<I.GAME_WIDTH-2; j++)
        {
            printw("%c",I.tabl[i][j]);
        }
        printw("\n");
    }
    refresh();
    getch();
    clear();
    for(i=0; i<I.GAME_HEIGHT-2; i++)
    {
        for(j=0; j<I.GAME_WIDTH-2; j++)
        {
            printw("%d",I.tab_k[i][j]);
        }
        printw("\n");
    }
    refresh();
    getch();
    clear();
    for(i=0; i<I.GAME_HEIGHT; i++)
    {
        for(j=0; j<I.GAME_WIDTH; j++)
        {
            printw("%c",I.colide[i][j]);
        }
        printw("\n");
    }
    refresh();
    getch();
    clear();
    for(i=0; i<I.GAME_HEIGHT-2; i++)
    {
        for(j=0; j<I.GAME_WIDTH-2; j++)
        {
            printw("%d",I.tab_id[i][j]);
        }
        printw("\n");
    }

    refresh();
    getch();
    nodelay(stdscr,1);
}

//*******************************************************************

void Interface::ingame_menu(HSTREAM hmus)
{
    char key;
    short updown=0;

    bool stop=false;

    time_t start;


    start=clock();

    BASS_ChannelPause(hmus);

    do
    {
        clear();

        if((key=='s'||key==2)&&updown<4)
            updown+=1;
        else if ((key=='w'||key==3)&&updown>0)
            updown-=1;

        attron( A_BOLD );
        mvprintw(8+(updown*2),WIN_WIDTH/2-8,"*");
        mvprintw(8,WIN_WIDTH/2-3,"Wzn\242w");
        mvprintw(10,WIN_WIDTH/2-3,"Zapis");
        mvprintw(12,WIN_WIDTH/2-3,"Opcje");
        mvprintw(14,WIN_WIDTH/2-6,"Menu g\210\242wne");
        //mvprintw(14,WIN_WIDTH/2-4,"Zako\344cz");
        mvprintw(16,WIN_WIDTH/2-3,"Debug");
        attroff( A_BOLD );

        key=getch();

        if(updown==4&&key==10)
        {
            updown=0;
            key=0;
            show_arrays(*this);
        }

        if(updown==3&&key==10)
        {
            loopend=false;
            stop=true;
        }


        if(updown==0&&key==10)
        {
            stop=true;
        }

        if(updown==1&&key==10)
        {
            menu_save();
        }

        if(updown==2&&key==10)
        {
            updown=0;
            do
            {
                clear();
                attron( A_BOLD );

                if( ( (key=='s'||key==2)&&updown<2)&&(musiconoff==true||updown==0) )
                    updown+=1;
                else if ((key=='w'||key==3)&&updown>0)
                    updown-=1;

                mvprintw(11+(updown*2),WIN_WIDTH/2-12,"*");

                mvprintw(11,WIN_WIDTH/2-8,"FPS(0/1):%d",fpsonoff);
                mvprintw(13,WIN_WIDTH/2-8,"Muzyka(0/1):%d",musiconoff);
                if(musiconoff==true)
                    mvprintw(15,WIN_WIDTH/2-8,"G\210o\230no\230\206:(%2.0f)",volume*100);

                key=getch();
                if(musiconoff==true)
                {
                    if(updown==2&&(key=='a'||key==4)&&volume>0.1)
                        volume-=0.10;
                    if(updown==2&&(key=='d'||key==5)&&volume<1)
                        volume+=0.10;
                }

                if(updown==1&&(key=='d'||key==5))
                    musiconoff=true;
                if(updown==1&&(key=='a'||key==4))
                    musiconoff=false;

                if(updown==0&&(key=='d'||key==5))
                    fpsonoff=true;
                if(updown==0&&(key=='a'||key==4))
                    fpsonoff=false;

                attroff( A_BOLD );
            }
            while(key!=27&&key!=10);
        }

    if(musiconoff==true)
    BASS_ChannelSetAttribute(hmus,BASS_ATTRIB_VOL,volume);

    }
    while(!stop);

    if(musiconoff==true)
    BASS_ChannelPlay(hmus, false);

    (diff)=(diff)+difftime(clock(),start);
}

//************************************************************************

void Interface::interfac()
{
    short znak,key;
    noecho(); //getch nie wypisuje nacisnietego klawisza na ekran
    nodelay(stdscr,1); //program nie zatrzymuje się przy getch, gdy nic nie zostanie nacisnięte getch zwraca -1

    short i,j,i_ran,x_tmp=1,y_tmp=2;
    short id_tmp, m_x, m_y, e_x, e_y, last,last_a,last_mv,last_rest; //zmienne last to blokady wykonania sie kilkukrotnie instrukcji w czasie jednego cyklu "zegara gry"
    short atk_speed=0,ene_speed=0; //atk_speed szybkosc ataku gracza,ene_speed szybkosc ataku wroga

    bool magic=false;
    bool attack=false;
    bool compare=false;
    bool rest=false;
    bool win=false;

    Player bobo;
    bobo.imie="Gracz";

    vector < Enemy* > ene;
    Enemy *new_ene;
    Enemy *wsk,*k_wsk=NULL;

    dial_push("Prawy przycisk myszy na obiekcie - informacja, Lewy przycisk myszy - akcja/atak",1);
    dial_push("z/x przes\242wanie wpis\242w w okienku informacyjnym",1);
    dial_push("F rzu\206 zakl\251cie, C porownuje sile przeciwnika z graczem, R regeneruj sily",1);
    dial_push("U dodaje nowego przeciwnika, K kopiuje ostatniego kliknietego istniejacego przzeciwnika",1);

    clear();

    tabl[y][x]='*';

    game_win = subwin(stdscr,GAME_HEIGHT-2,GAME_WIDTH-2,2,1);
    score = subwin(stdscr,GAME_HEIGHT-15,19,2,GAME_WIDTH);
    enemy_info = subwin(stdscr,GAME_HEIGHT-21,19,GAME_HEIGHT-12,GAME_WIDTH);
    dialog = subwin(stdscr,5,WIN_WIDTH-2,GAME_HEIGHT+1,1);

    keypad(game_win,true);
    MEVENT event_mouse;
    mousemask(ALL_MOUSE_EVENTS, NULL);

    draw_borders();

    if(is_game_loaded)
    {
        for(i=0; i<GAME_HEIGHT-2; i++)
            for(j=0; j<GAME_WIDTH-2; j++)
                {
                if(tabl[i][j]=='B')
                    {
                    new_ene = new Bandit(*this);
                    new_ene->it=ene.size();
                    ene.push_back( new_ene );
                    tabl[i][j]=' ';
                    }
                if(tabl[i][j]=='G')
                    {
                    new_ene = new Goblin(this);
                    new_ene->it=ene.size();
                    ene.push_back( new_ene );
                    tabl[i][j]=' ';
                    }
                }

    }
    else
    {
        i_ran=rand() % 4 + 20;
        for(i=1; i<=i_ran; i++)
            los_tree();

    new_ene = new Bandit(*this);
    new_ene->it=0;
    ene.push_back( new_ene );

    new_ene = new Goblin(this);
    new_ene->it=1;
    ene.push_back( new_ene );
    }
//***************************SOUND***************************

        HSTREAM hmus;
        BASS_Init(-1, 44100, 0, 0,0);
        BASS_Start();

        hmus = BASS_StreamCreateFile(false,"theme.mp3", 0, 0, 0);
        if(musiconoff==true)
        {
        //BASS_SetVolume(volume);
        BASS_ChannelSetAttribute(hmus,BASS_ATTRIB_VOL,volume); znak=219;
        BASS_ChannelPlay(hmus, true);
        }

//************************************************************

    start_color();
    init_pair( 1, COLOR_WHITE, COLOR_BLACK ); //biały
    init_pair( 2, 12, COLOR_BLACK ); //jaskrawy czerwony
    init_pair( 3, 14, COLOR_BLACK ); //jaskrawy żołty
    init_pair( 4, COLOR_GREEN, COLOR_BLACK ); //zielony
    init_pair( 5, 4 , COLOR_BLACK );  //brązowy
    init_pair( 6, 3 , COLOR_BLACK );
    init_pair( 7, 10 , COLOR_BLACK ); //jaskrawy zielony
    init_pair( 8, COLOR_WHITE , COLOR_YELLOW ); //biały żółte tło
//1 niebieski ciemny,2 zielony jasny,3 niebieski morski,4 brąz,5 fiolet,6 żółty sraczkowaty,7 biały,8 szary,9 niebieski jaskrawy,
//10 jaskrawy zielony,11 niebieski jasny,12 jaskrawy czerwony,13 różowy,14 jaskrawy żółty,15 biały jasny, 16


    refresh();
    beg=clock();
    dial_draw();

    do //główna pętla gry
    {
        wclear(game_win);

        fps++; //zlicza ilość wykonań pętli

        draw_score_win(&bobo);
        draw_game();


        if(atk_sec!=last_mv)//znak gdy nie jest naciśnięty zwraca -1 przy każdym obiegu pętli
        {
            last_mv=atk_sec;
//p_speed++;
            // if(p_speed==10) //modyfikowanie predkosci gracza
            // {
//       p_speed=0;
            if((znak=='w'||znak==KEY_UP)&& colide[y][x+1]!='T')
            {
                y-=1;
                znak=0;
                rest=false;
            }
            if((znak=='s'||znak==KEY_DOWN)&& colide[y+2][x+1]!='T')
            {
                y+=1;
                znak=0;
                rest=false;
            }
            if((znak=='a'||znak==KEY_LEFT)&& colide[y+1][x]!='T')
            {
                x-=1;
                znak=0;
                rest=false;
            }
            if((znak=='d'||znak==KEY_RIGHT)&& colide[y+1][x+2]!='T')
            {
                x+=1;
                znak=0;
                rest=false;
            }
//   }
        }

        key=wgetch(game_win);
        if(key!=-1) //jesli nic nie zostalo nacisniete wgetch zwraca -1 więc tylko w przypadku nacisnięcia zostanie nadpisana zmienna znak
            znak=key;

        nc_getmouse(&event_mouse);
        if(event_mouse.bstate&BUTTON1_CLICKED)
        {
            m_x=event_mouse.x-1;
            m_y=event_mouse.y-2;
            if(m_x>0&&m_y>0&&m_x<GAME_WIDTH&&m_y<GAME_HEIGHT)
            {

                if(tabl[m_y][m_x]=='B' || tabl[m_y][m_x]=='G' || tabl[m_y][m_x]=='D')
                {
                    e_x=m_x; //przypisanie pozycji myszy do pozycji wroga
                    e_y=m_y;
                    id_tmp = tab_id[m_y][m_x];
                    call_enemy(ene[id_tmp]);
                    k_wsk = ene[id_tmp];
                    if(magic==false)
                    {
                        if(compare)
                        {
                            wsk=ene[id_tmp];
                            if(bobo==wsk)
                            {
                                dial_push("Przeciwnik jest s\210abszy od ciebie",1);
                            }
                            else
                                dial_push("Przeciwnik jest silniejszy od ciebie",1);
                            compare=false;
                        }
                        else if(sqrt((x - m_x)*(x - m_x)+(y - m_y)*(y - m_y))<2)
                        {
                            wsk=ene[id_tmp];
                            attack=true;
                        }
                        else
                        {
                            dial_push("Jeste\230 zbyt daleko \276eby zatakowa\206",1);
                            //wprintw(enemy_info,"%d",ene[id_tmp]->it);
                            //wrefresh(enemy_info);
                        }


                    }
                    else
                    {
                        if(ene[id_tmp]->hp>0)
                        {
                            dial_push("Kula ognia zada\210a 20 pkt. obra\276e\344",4);
                            ene[id_tmp]->enemy_hit( 20 );
                            bobo.mp-=10;
                            call_enemy(ene[id_tmp]);
                            magic=false;
                        }
                        else
                            dial_push("Zabi\210es go",1);

                    }
                }
                if(tabl[m_y][m_x]=='X')
                {
                    dial_push("Przeszukujesz pokonanego i znajdujesz zio\210a lecznicze (+40 HP)",1);
                    bobo.hp+=40;
                    tabl[m_y][m_x]=' ';
                    tab_k[m_y][m_x]=1;
                    colide[m_y+1][m_x+1]=' ';
                }

            }

        }
        if(event_mouse.bstate&BUTTON3_CLICKED)
        {
            m_x=event_mouse.x-1;
            m_y=event_mouse.y-2;
            if(tabl[m_y][m_x]=='B')
            {
                dial_push("To jest Bandyta",1);
            }
            if(tabl[m_y][m_x]=='G')
            {
                dial_push("To jest Goblin",1);
            }
            if(tabl[m_y][m_x]=='D')
            {
                dial_push("To jest Stra\276nik",1);
            }
            if(tabl[m_y][m_x]=='X')
            {
                dial_push("Tu le\276y pokonany przeciwnik",1);
            }
        }


        if(magic==true)
        {
            if(  (event_mouse.y!=y_tmp) || (event_mouse.x!=x_tmp)  ) //jeżeli miejsce na które wskazuje myszka jest różne od miejsca na które poprzednio wskazywała
            {
                if(event_mouse.y>1 && event_mouse.x>0 && event_mouse.y<GAME_HEIGHT && event_mouse.x<GAME_WIDTH-1)
                {
                    tab_k[event_mouse.y-2][event_mouse.x-1]=8;
                    tab_k[y_tmp-2][x_tmp-1]=1;
                    y_tmp=event_mouse.y;
                    x_tmp=event_mouse.x;
                }
            }
        }

        if(znak=='f')
        {
            if(bobo.mp>0)
            {
                if(magic==false)
                    magic=true;
                else
                {
                    magic=false;
                    tab_k[y_tmp-2][x_tmp-1]=1;
                }
            }
            else
                dial_push("Masz za ma\210o many",1);

            znak=0;
        }


        if(atk_sec!=last)
        {
            last=atk_sec;
            ene_speed++;

            if(ene_speed==20)
            {
                ene_speed=0;
                short dmg;
                string tmp;
                string str;
                for(unsigned short iter=0; iter<ene.size(); iter++)
                {
                    try
                    {
                        if(ene[iter]->life_cycle(this,y,x)==1)
                        {
                            dmg=ene[iter]->enemy_atk();
                            sprintf((char*)tmp.c_str(), "%d", dmg);
                            str = tmp.c_str();
                            dial_push("Otrzyma\210e\230 "+str+" pkt. obra\276e\344",2);
                            bobo.hit(dmg);
                        }
                    }
                    catch(string e)
                    {
                        dial_push(e,2);
                        ene.erase(ene.begin() + iter);
                        for(unsigned short n_iter=0; n_iter<ene.size(); n_iter++)
                        {
                            ene[n_iter]->it=n_iter;
                        }
                        break;
                    }
                }
            }
        }

        if(rest==true)
        {
            if(atk_sec!=last_rest)
            {
                last_rest=atk_sec;
                if(atk_sec%20==0)
                {
                    if(bobo.hp<(100+(bobo.lvl*10)))
                        bobo.hp++;
                }
            }
        }

        if(compare==true)
        {
            if(  (event_mouse.y!=y_tmp) || (event_mouse.x!=x_tmp)  ) //jeżeli miejsce na które wskazuje myszka jest różne od miejsca na które poprzednio wskazywała
            {
                if(event_mouse.y>1 && event_mouse.x>0 && event_mouse.y<GAME_HEIGHT && event_mouse.x<GAME_WIDTH-1)
                {
                    tab_k[event_mouse.y-2][event_mouse.x-1]=8;
                    tab_k[y_tmp-2][x_tmp-1]=1;
                    y_tmp=event_mouse.y;
                    x_tmp=event_mouse.x;
                }
            }
        }

        if(attack==TRUE)
        {
            if(atk_sec!=last_a)
            {
                last_a=atk_sec;
                atk_speed++;
                if(atk_speed==20)
                {
                    atk_speed=0;
                    if(sqrt((x - e_x)*(x - e_x)+(y - e_y)*(y - e_y))<2) //tu będzie trzeba zmienić warunek by zczytywało aktualną pozycje wroga bo w przypadku ruchu wroga ten warunek będzie błędny
                    {
                        short dmg;
                        dmg=bobo.attack();
                        string tmp;
                        sprintf((char*)tmp.c_str(), "%d", dmg);
                        string str = tmp.c_str();
                        if(wsk->hp-dmg>0)
                        {
                            dial_push("Zada\210e\230 "+str+" pkt. obra\276e\344",4);
                            wsk->enemy_hit( dmg ); //wysłanie informacji o obrażeniach do metody odejmującej punkty życia
                            call_enemy(wsk); //wyświetlenie zaktualizowanych informacji o wrogu
                        }
                        else
                        {
                            attack=false;
                            wsk->hp=0;
                            call_enemy(wsk);
                            dial_push("Zada\210e\230 "+str+" pkt. obra\276e\344 i pokona\210e\230 go",4);

                            bobo.pd+=wsk->pd;
                            if(bobo.pd>=100)
                            {
                                bobo.pd-=100;
                                bobo.lvl++;
                                if(bobo.lvl==3)
                                {
                                    new_ene = new Guard(*this);
                                    new_ene->it=ene.size();
                                    ene.push_back( new_ene );
                                }
                            }

                            if(wsk->obj_id==1)
                                {
                                  loopend=false;
                                  win=true;
                                }



                            tabl[wsk->pos_y][wsk->pos_x]='X';
                            ene.erase(ene.begin() + wsk->it);
                            k_wsk=NULL;
                            for(unsigned short iter=0; iter<ene.size(); iter++)
                            {
                                ene[iter]->it=iter;
                            }

                            if((int)(rand() / (RAND_MAX + 1.0) * 2 )==0)
                            {
                                new_ene = new Bandit(*this);
                                new_ene->it=ene.size();
                                ene.push_back( new_ene );
                            }

                            else
                            {
                                new_ene = new Goblin(this);
                                new_ene->it=ene.size();
                                ene.push_back( new_ene );
                            }

                        }
                    }
                    else
                        attack=FALSE;
                }
            }
        }


        tabl[y][x]='*'; //przypisanie do tablicy znacznika postaci

        if(znak==27)
        {
            ingame_menu(hmus); //menu w trakcie działania gry
            znak=0; //zerowanie znaku 27 z zmiennej znak. Bez tego if spełnia się za każdym razem i nie można wyjść z menu
            draw_borders(); //rysuje ramkę
            dial_draw(); //rysuje okno informacyjne
            refresh(); //odswierza
        }

        if(znak=='u')
        {
            if(ene.size()>40)
                dial_push("Nie mozna wygenerowac wiecej niz 40 przeciwnikow",2);
            else
            {
                if((int)(rand() / (RAND_MAX + 1.0) * 2 )==0)
                {
                    new_ene = new Bandit(*this);
                    new_ene->it=ene.size();
                    ene.push_back( new_ene );
                }

                else
                {
                    new_ene = new Goblin(this);
                    new_ene->it=ene.size();
                    ene.push_back( new_ene );
                }

            }
            znak=0;
        }

        if(znak=='r')
        {
            rest=true;
            znak=0;
        }

        if(znak=='c')
        {
            if(compare==false)
                compare=true;
            else
            {
                compare=false;
                tab_k[y_tmp-2][x_tmp-1]=1;
            }
            znak=0; //zerowanie znaku z zmiennej znak. Bez tego if spełnia się za każdym razem i nie można wyjść z menu
        }

        if(znak=='k')
        {
            if(k_wsk!=NULL)
            if(k_wsk->avatar=='B')
            {
                new_ene = new Bandit(*this,*k_wsk);
                new_ene->it=ene.size();
                ene.push_back( new_ene );
            }
            else
            {
                new_ene = new Goblin(*this,*k_wsk);
                new_ene->it=ene.size();
                ene.push_back( new_ene );
            }


            znak=0; //zerowanie znaku z zmiennej znak. Bez tego if spełnia się za każdym razem i nie można wyjść z menu
        }

        if(znak=='e')
        {
            inwentory(); //menu w trakcie działania gry
            znak=0; //zerowanie znaku z zmiennej znak. Bez tego if spełnia się za każdym razem i nie można wyjść z menu
            draw_borders();
            dial_draw();
            refresh();
        }

        if(znak=='z')
        {
            if(start_pos->next!=NULL)
                start_pos=start_pos->next;

            dial_draw();
            znak=0; //zerowanie znaku z zmiennej znak. Bez tego if spełnia się za każdym razem i nie można wyjść z menu
        }

        if(znak=='x')
        {
            start_pos=lista;
            dial_draw();
            znak=0;
        }

    }
    while(bobo.hp > 0 && loopend);

//        BASS_Stop();
//        BASS_Free();
    clear();
    ene.clear(); //usuniecie wszystkich obiektow z wektora
    clean();//czyszczenie zmniennych

    if(win)
    {
        attron( A_BOLD );
        mvprintw(WIN_HEIGHT/2,WIN_WIDTH/2-17,"Ukonczyles gre w czasie %dm:%.0fs ",minutes,seconds);
        attroff( A_BOLD );
        nodelay(stdscr,0);
        getch();
        nodelay(stdscr,1);
    }
    if(bobo.hp<0)
    {
        attron( A_BOLD );
        mvprintw(WIN_HEIGHT/2,WIN_WIDTH/2-5,"Koniec gry");
        attroff( A_BOLD );
        nodelay(stdscr,0);
        getch();
        nodelay(stdscr,1);
    }

}

//************************************************************************

void Interface::call_enemy(Enemy *bandyta)
{
    bandyta->draw_eninfo_win(enemy_info);
}

//************************************************************************

void Interface::dial_push(string new_dialogue,short dia_colour)
{
    dialogue *tmp;

    tmp=new dialogue;
    tmp->txt=new_dialogue;
    tmp->col=dia_colour;
    tmp->next=lista;
    lista=tmp;

    start_pos=lista;

    dial_draw();
}

//************************************************************************

void Interface::save(char save_name[25])
{
    char file_name[25];
    short i,j;
    time_t save_beg_temp;

    strcpy( file_name, "");
    strcat( file_name, "save/");
    strcat(file_name,save_name);
    strcat( file_name, ".dnb");

    save_beg_temp=save_beg;
    save_beg=zegar;

    std::fstream save;
    save.open(file_name,std::ios::out);

    for(i=0; i<GAME_HEIGHT-2; i++) //zapisywanie tablicy znakow
    {
        for(j=0; j<GAME_WIDTH-2; j++)
        {
            save<<tabl[i][j];
        }
        save<<"\n";
    }

    for(i=0; i<GAME_HEIGHT-2; i++) //zapisywanie tablicy kolorow
    {
        for(j=0; j<GAME_WIDTH-2; j++)
        {
            save<<tab_k[i][j];
        }
        save<<"\n";
    }

    for(i=0; i<GAME_HEIGHT-2; i++) //zapisywanie tablicy id
    {
        for(j=0; j<GAME_WIDTH-2; j++)
        {
            save<<tab_id[i][j];
        }
        save<<"\n";
    }

    for(i=0; i<GAME_HEIGHT; i++) //zapisywanie tablicy kolizji
    {
        for(j=0; j<GAME_WIDTH; j++)
        {
            save<<colide[i][j];
        }
        save<<"\n";
    }

    save<<x<<" "<<y<<" "<<save_beg;

    attron(A_BOLD);
    mvprintw(WIN_HEIGHT-3,WIN_WIDTH/2-10,"Gra zosta\210a zapisana");
    attroff(A_BOLD);

    save_beg=save_beg_temp;
    getch();
    save.close();
}

//**********************************************************************

void Interface::load(char save_name[25])
{
    char file_name[25];
    strcpy( file_name, "");
    strcat( file_name, "save/");
    strcat(file_name,save_name);
    strcat( file_name, ".dnb");

    short i,j;
    std::fstream load;
    load.open(file_name,std::ios::in);
    char odczyt[GAME_WIDTH-1];
    is_game_loaded=true;
//clear();

    for(i=0; i<GAME_HEIGHT-2; i++)
    {
        load.getline(odczyt,GAME_WIDTH-1);
        for(j=0; j<GAME_WIDTH-2; j++)
        {
            tabl[i][j]=odczyt[j];
        }
    }

    for(i=0; i<GAME_HEIGHT-2; i++)
    {
        load.getline(odczyt,GAME_WIDTH-1);
        for(j=0; j<GAME_WIDTH-2; j++)
        {
            tab_k[i][j]=odczyt[j]-48;
        }
    }

    for(i=0; i<GAME_HEIGHT-2; i++)
    {
        load.getline(odczyt,GAME_WIDTH-1);
        for(j=0; j<GAME_WIDTH-2; j++)
        {
            tab_id[i][j]=odczyt[j]-48;
        }
    }

    for(i=0; i<GAME_HEIGHT; i++)
    {
        load.getline(odczyt,GAME_WIDTH+1);
        for(j=0; j<GAME_WIDTH; j++)
        {
            colide[i][j]=odczyt[j];
        }
    }

    load>>x>>y>>save_beg;

    //attron(A_BOLD);
    //mvprintw(WIN_HEIGHT/2,WIN_WIDTH/2-10,"savebeg: %d",save_beg);
    //attroff(A_BOLD);
    //getch();

    load.close();
}

//**********************************************************************

void Interface::menu_save()
{
    nodelay(stdscr,0);
    short updown;
    char key;
    std::fstream save_name,load_name;

    updown=0;
    key=0;

    load_name.open("save/savelist.dnb",std::ios::in);
    load_name.getline(slot1,25);
    load_name.getline(slot2,25);
    load_name.getline(slot3,25);
    load_name.close();
    save_name.open("save/savelist.dnb",std::ios::out);
    do
    {
        clear();
        attron( A_BOLD );

        if((key=='s'||key==2)&&updown<2)
            updown+=1;
        if ((key=='w'||key==3)&&updown>0)
            updown-=1;

        mvprintw(10+(updown*2),WIN_WIDTH/2-9,"*");
        mvprintw(10,WIN_WIDTH/2-6,"->");
        mvprintw(12,WIN_WIDTH/2-6,"->");
        mvprintw(14,WIN_WIDTH/2-6,"->");

        mvprintw(WIN_HEIGHT-1,WIN_WIDTH/2-32,"Enter - zapisz");
        mvprintw(WIN_HEIGHT-1,WIN_WIDTH/2-8,"r - zmie\344 nazw\251");
        mvprintw(WIN_HEIGHT-1,WIN_WIDTH/2+15,"x - usu\344 zapis");

        if(strlen(slot1)==0)
            mvprintw(10,WIN_WIDTH/2-4,"PUSTO");
        else
            mvprintw(10,WIN_WIDTH/2-4,"%s",slot1);

        if(strlen(slot2)==0)
            mvprintw(12,WIN_WIDTH/2-4,"PUSTO");
        else
            mvprintw(12,WIN_WIDTH/2-4,"%s",slot2);

        if(strlen(slot3)==0)
            mvprintw(14,WIN_WIDTH/2-4,"PUSTO");
        else
            mvprintw(14,WIN_WIDTH/2-4,"%s",slot3);


        key=getch();
//***********************************************************

        if(updown==0&&key==10)
        {
            submenu_save_ENTER(slot1,&updown);
        }

        if(updown==0&&key=='r'&&strlen(slot1)!=0)
        {
            submenu_save_rename(slot1,&updown);
        }

        if(updown==0&&key=='x'&&strlen(slot1)!=0)
        {
            submenu_save_delete(slot1,&updown);
        }

//************************************************************

        if(updown==1&&key==10)
        {
            submenu_save_ENTER(slot2,&updown);
        }

        if(updown==1&&key=='r'&&strlen(slot2)!=0)
        {
            submenu_save_rename(slot2,&updown);
        }

        if(updown==1&&key=='x'&&strlen(slot2)!=0)
        {
            submenu_save_delete(slot2,&updown);
        }

//************************************************************

        if(updown==2&&key==10)
        {
            submenu_save_ENTER(slot3,&updown);
        }

        if(updown==2&&key=='r'&&strlen(slot3)!=0)
        {
            submenu_save_rename(slot3,&updown);
        }

        if(updown==2&&key=='x'&&strlen(slot3)!=0)
        {
            submenu_save_delete(slot3,&updown);
        }

//************************************************************

        attroff( A_BOLD );
    }
    while(key!=27);
    key=0;
    save_name<<slot1;
    save_name<<"\n";
    save_name<<slot2;
    save_name<<"\n";
    save_name<<slot3;
    save_name<<"\n";
    save_name.close();

    load_name.close();

    nodelay(stdscr,1);
}

//************************************************************************

void Interface::submenu_save_ENTER(char slot[25],short *updown)
{
    if(strlen(slot)!=0)
        save(slot);
    else
    {
        echo();
        curs_set(1);
        move(10+(*updown*2),WIN_WIDTH/2-4);
        getstr(slot);

        if(strlen(slot)==0)
            strcpy(slot,"");
        else
            save(slot);

        curs_set(0);
        noecho();
    }
}

void Interface::submenu_save_rename(char slot[25],short *updown)
{
    char slot_s[50],slot_tmp[25];
    echo();
    curs_set(1);
    move(10+(*updown*2),WIN_WIDTH/2-4);
    strcpy(slot_tmp,slot);
    getstr(slot);
    if(strlen(slot)==0)
    {
        strcpy(slot_s,"");
        strcat(slot_s,"del save\\");
        strcat(slot_s,slot_tmp);
        strcat(slot_s,".dnb ");
        system(slot_s);
        strcpy(slot,"");
    }
    else
    {
        strcpy(slot_s,"");
        strcat(slot_s,"ren save\\");
        strcat(slot_s,slot_tmp);
        strcat(slot_s,".dnb ");
        strcat(slot_s,slot);
        strcat(slot_s,".dnb");
        system(slot_s);
    }
    curs_set(0);
    noecho();
}

void Interface::submenu_save_delete(char slot[25],short *updown)
{
    char slot_s[50];

    echo();
    curs_set(1);
    strcpy(slot_s,"");
    move(10+(*updown*2),WIN_WIDTH/2-4);
    strcat(slot_s,"del save\\");
    strcat(slot_s,slot);
    strcat(slot_s,".dnb ");
    system(slot_s);
    strcpy(slot,"");
    curs_set(0);
    noecho();
}

void Interface::draw_borders()
{
    short i;

    attron( A_BOLD );
    mvprintw(0,WIN_WIDTH/2-12,"Dungeons & Bobos V.2.0");
    attroff( A_BOLD );

    mvprintw(1,0,"%c",201); //lewy górny narożnik
    mvprintw(GAME_HEIGHT,0,"%c",204);
    mvprintw(1,GAME_WIDTH-1,"%c",203);
    mvprintw(GAME_HEIGHT,GAME_WIDTH-1,"%c",202);

    for(i=2; i<GAME_HEIGHT; i++)
    {
        mvprintw(i,0,"%c",186);
        mvprintw(i,GAME_WIDTH-1,"%c",186);
    }

    for(i=1; i<GAME_WIDTH-1; i++)
    {
        mvprintw(1,i,"%c",205);
        mvprintw(GAME_HEIGHT,i,"%c",205);
    }

    //*************************************

    mvprintw(1,WIN_WIDTH-1,"%c",187);
    mvprintw(GAME_HEIGHT,WIN_WIDTH-1,"%c",185);

    for(i=2; i<GAME_HEIGHT; i++)
    {
        mvprintw(i,WIN_WIDTH-1,"%c",186);
    }

    for(i=GAME_WIDTH; i<WIN_WIDTH-1; i++)
    {
        mvprintw(1,i,"%c",205);
        mvprintw(GAME_HEIGHT-13,i,"%c",205);
        mvprintw(GAME_HEIGHT,i,"%c",205);
    }
    mvprintw(GAME_HEIGHT-13,WIN_WIDTH-1,"%c",185);
    mvprintw(GAME_HEIGHT-13,GAME_WIDTH-1,"%c",204);
    //*************************************

    mvprintw(WIN_HEIGHT-1,0,"%c",200);
    mvprintw(WIN_HEIGHT-1,WIN_WIDTH-1,"%c",188);

    for(i=GAME_HEIGHT+1; i<WIN_HEIGHT-1; i++)
    {
        mvprintw(i,0,"%c",186);
        mvprintw(i,WIN_WIDTH-1,"%c",186);
    }

    for(i=1; i<WIN_WIDTH-1; i++)
    {
        mvprintw(WIN_HEIGHT-1,i,"%c",205);
    }

}

//**********************************************************************

void Interface::draw_game()
{
    short i,j;
    for(i=0; i<GAME_HEIGHT-2; i++)
    {
        for(j=0; j<GAME_WIDTH-2; j++)
        {
            wattron(game_win,COLOR_PAIR(tab_k[i][j]));
            wprintw(game_win,"%c",tabl[i][j]);
            wattroff(game_win,COLOR_PAIR(tab_k[i][j]));
        }
    }
    tabl[y][x]=' ';

    wrefresh(game_win);
}

//************************************************************************

void Interface::draw_score_win(Player *bobo)
{
    wclear(score);

    zegar=clock() - beg - diff + save_beg; //zegar gry po odjęciu czasu z początku aplikacji i czasu spędzonego w menu a także po dodaniu zapisanego czasu

    mvwprintw(score,1,1,"Imie: %s",bobo->imie.c_str());
    mvwprintw(score,3,1,"Poziom: %d",bobo->lvl);
    mvwprintw(score,5,1,"HP: %d",bobo->hp);
    mvwprintw(score,7,1,"MP: %d",bobo->mp);
    mvwprintw(score,9,1,"PD: %d",bobo->pd);

    if((short)seconds%10==0)
        tmp_sec=seconds;

    atk_sec=(seconds-tmp_sec)*10;
    //mvwprintw(score,7,1,"czas %d",atk_sec);
    seconds=(double)zegar/1000;
    if(seconds>59)
    {
        minutes++;
        beg = clock() - diff;
    }


    mvwprintw(score,16,1,"Czas => %dm:%.0fs",minutes,seconds);


    if(fpsonoff==true)
    {
        czas=clock()-cykl; //ilość cykli zegara obecnie minus ilość cykli z poprzeniej sekundy co powoduje zerowanie czasu po przekroczeniu sekundy
        s=clock();

        sec=(double)czas/1000;

        if(sec>1) //jeśli zegar przekroczy sekundę to się zeruje
        {
            cykl=s; //przypisanie obecnej liczby cykli w celu późniejszego odjęcia jej od obecnej ilośći tak by zegar liczył od zera
            f=fps; //przypisanie liczby obrotów pętli w ciągu sekundy
            fps=0; //zerowanie licznika obrotow pętli
        }

        mvwprintw(stdscr,0,90,"FPS => %d",f);
        wrefresh(stdscr);
    }


    wrefresh(score);

}

//************************************************************************

void Interface::dial_draw()
{
    wclear(dialog);
    dialogue *tmp=start_pos;

    if(tmp!=NULL)
    {
        for(int i=0; i<5; i++)
        {
            wattron(dialog,COLOR_PAIR(tmp->col));
            wprintw(dialog,"%s\n",tmp->txt.c_str());
            wattroff(dialog,COLOR_PAIR(tmp->col));

            if(tmp->next==NULL)
                break;

            tmp=tmp->next;
        }
    }

    wrefresh(dialog);
}

//**********************************************************************

void Interface::los_tree()
{
    short x,y,good;
    do
    {
        good=0;
        x= (int)(rand() / (RAND_MAX + 1.0) * (GAME_WIDTH-8) )+4;
        y= (int)(rand() / (RAND_MAX + 1.0) * (GAME_HEIGHT-8) )+5;

        if(tabl[y][x]==' ')
            good++;
        if(tabl[y-1][x]==' ')
            good++;
        if(tabl[y-1][x-1]==' ')
            good++;
        if(tabl[y-1][x+1]==' ')
            good++;
        if(tabl[y-2][x]==' ')
            good++;
        if(tabl[y-2][x-1]==' ')
            good++;
        if(tabl[y-2][x+1]==' ')
            good++;
        if( tabl[y-2][x-2]==' ')
            good++;
        if(tabl[y-2][x+2]==' ')
            good++;
        if(tabl[y-3][x+1]==' ')
            good++;
        if(tabl[y-3][x-1]==' ')
            good++;
        if(tabl[y-3][x]==' ')
            good++;

    }
    while(good!=12);

    tabl[y][x]=219;
    tab_k[y][x]=5;
    colide[y+1][x+1]='T';

    tabl[y-1][x]=219;
    tab_k[y-1][x]=5;
    colide[y][x+1]='T';

    tabl[y-1][x-1]='*';
    tab_k[y-1][x-1]=4;
    colide[y][x]='T';

    tabl[y-1][x+1]='*';
    tab_k[y-1][x+1]=4;
    colide[y][x+2]='T';

    tabl[y-2][x]='*';
    tab_k[y-2][x]=4;
    colide[y-1][x+1]='T';

    tabl[y-2][x-1]='*';
    tab_k[y-2][x-1]=4;
    colide[y-1][x]='T';

    tabl[y-2][x+1]='*';
    tab_k[y-2][x+1]=4;
    colide[y-1][x+2]='T';

    tabl[y-2][x-2]='*';
    tab_k[y-2][x-2]=4;
    colide[y-1][x-1]='T';

    tabl[y-2][x+2]='*';
    tab_k[y-2][x+2]=4;
    colide[y-1][x+3]='T';

    tabl[y-3][x+1]='*';
    tab_k[y-3][x+1]=4;
    colide[y-2][x+2]='T';

    tabl[y-3][x-1]='*';
    tab_k[y-3][x-1]=4;
    colide[y-2][x]='T';

    tabl[y-3][x]='*';
    tab_k[y-3][x]=4;
    colide[y-2][x+1]='T';

}

//**********************************************************************

void Interface::clean()
{
    int i,j;
    for(i=0; i<GAME_HEIGHT-2; i++)
        for(j=0; j<GAME_WIDTH-2; j++)
           {
            tabl[i][j]=' ';
            tab_k[i][j]=1;
            tab_id[i][j]=0;
           }

    for(i=1; i<GAME_HEIGHT-2; i++)
        for(j=1; j<GAME_WIDTH-2; j++)
            colide[i][j]=' ';


    lista=NULL;
    start_pos=NULL;

    diff=0;
    save_beg=0;
    czas=0;
    cykl=0;
    s=0;
    beg=0;
    zegar=0;

    sec=0;

    is_game_loaded=false;
    loopend=true;

    x=0;
    y=0;

    minutes=0;
    hour=0;
    seconds=0;
}
