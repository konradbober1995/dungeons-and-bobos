#ifndef player_h
#define player_h
#include "interface.h"
#include "enemy.h"

class Enemy;

class Player
{
public:
    std::string imie;
    short lvl;
    short pd;
    short hp;
    short mp;
    short damage;

    bool operator ==(Enemy* ene);
    short attack();
    void rest();
    void hit(short h);
    Player();
};
#endif
